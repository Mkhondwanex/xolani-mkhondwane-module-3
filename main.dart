import 'dart:html';

import 'package:flutter/material.dart';
import 'LoginScreen.dart';


void main() {
  runApp( const LoginScreen());
}
class LoginScreen extends StatelessWidget{
 const LoginScreen({Key? key}) : super(key:key);
 
  @override
  Widget build(BuildContext context){
    return MaterialApp( 
      title: 'MTN_App_Academy_Login '
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: LoginScreen(),
    );
  }
}

  class  Dashboard extends StatelessWidget{
    const Dashboard ({Key? key}) : super (key:key);
  @override
Widget build(BuildContext context){
    return MaterialApp(
      debugShowCheckedModeBanner: false,
     title: 'MTN_App_Academy_Dashboard' 
     theme: ThemeData(
       primarySwatch: Colors.blue,
     ),
     home: Dashboard(),    
    );
 }
}